<?php
/**
 * @author MaitrePylos <info@formatux.be>
 */

require 'outils.php';
require 'dessin.php';

/**
 * Recherche un mot aléatoire et renvoie un tableau
 */
$mot = found('mots.txt');
$game = [
    'mot' => $mot,
    'occurrence' => array_fill(0, count($mot), '-'),
    'lettres' => range('a', 'z'),
    'jouer' => [],
    'coup' => 8
];

echo $titre;
do {

    echo dessinPendu($game['coup']);
    if($game['coup'] === 0){
        echo "\n"."Dommage, le mot était : " . implode('', $game['mot']);
        exit();
    }

    foreach ($game['occurrence'] as $oc) {
        echo $oc . " ";
    }
    echo "\n";
    echo "Lettre déjà joué : " . implode('-', $game['jouer']) . "\n";

    $choix = lireLettre($game['lettres'], $game['jouer']);
    $game['jouer'][] = $choix;

    if (in_array($choix, $game['mot'], true)) {
        $key = array_keys($game['mot'], $choix);
        foreach ($key as $k) {
            $game['occurrence'][$k] = $choix;
        }
        continue;
    }
    $game['coup']--;


} while ($game['mot'] !== $game['occurrence']);

echo 'Good Game : ' . implode('', $game['mot']);
