<?php

function found($file): array
{
    $mots = file($file);
    shuffle($mots);

    $mot = trim($mots[0]);
    $mot = strtolower($mot);
    $mot = stripAccents($mot);
    $mot = str_split($mot);

    return $mot;
}

/**
 * Remplace les caractères accentués par leurs homologues normaux.
 * @ajout des apostrophes et tirets remplacer par un espace.
 *
 * @param type $string
 * @return string
 */
function stripAccents($string): string
{
    $_remplace = [
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ü' => 'u',
        'ÿ' => 'y',
        'ñ' => 'n',
        'ç' => 'c',
        'ø' => '0',
        '\'' => '',
        '-' => ''
    ];
    return strtr((string)$string, $_remplace);
}

function lireLettre(array $lettres, array $jouer): ?string
{
    $sortie = false;
    $choix = null;
    while ($sortie !== true) {
        $choix = readline("Entrez votre lettre : ");
        $choix = strtolower(trim($choix));
        if (strlen($choix) > 1) {
            echo "vous ne pouvez jouer qu'une seule lettre" . "\n";
            continue;
        }
        if (!in_array($choix, $lettres, true)) {
            echo "vous ne pouvez jouer qu'une lettre comprise entre a et z" . "\n";
            continue;
        }
        if (in_array($choix, $jouer, true)) {
            echo "vous avez déjà joué la lettre $choix" . "\n";
            continue;
        }
        $sortie = true;
    }
    return $choix;
}